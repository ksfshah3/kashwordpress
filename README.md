# High Availability WP Cloudformation Template

To use this template with AWS CLI, you first have to create an IAM user with Administrative access and ensure it has adequate hardening.

You install AWS CLI on your machine, follow the [installation guide](https://aws.amazon.com/cli/).

On Mac, you can use Brew:

```
brew install awscli
```

With the IAM role properly set up, you can then proceed to running:

```
aws configure
```

For the sake of sanity and not to see errors, I will recommend using the link to this file on Gitlab or you upload the file to S3 
because the main template file is really huge.

## First purchase your domain on route 53

This is advisable if you don't already own a domain name:

I used kashautomation.com


## Get the Zone set up:

Run this in the directory you clone this repo to, remember the parameters files live in this directory.

```
aws cloudformation create-stack --stack-name WPZone --template-url https://kashautomaton.s3-us-west-2.amazonaws.com/zones.yml  --parameters file://zone-parameters.json
```

Except you use a local `.yml` file in which instead of `--template-url` you use `--template-body` and you pass the file path.

For the `--template-url`, be sure to pass an `S3` URL. 

Also take note the `--stack-name`, in this case I named it `WPZone`. If you call it something else, please update it when needed. 

Once the zone is done.

Update the DNS record of the domain to be served from **Route 53**, this will prevent the Certs or Cloudfront from having any issues.


## Get the VPC running

```
aws cloudformation create-stack --stack-name kashwpautomation --template-url https://kashautomaton.s3-us-west-2.amazonaws.com/vpc-2az.yml
```

Again, you can reference the file from repo.

## S3

Upload your `.yml` files into an s3 bucket after making required changes to them

## The Main Template

Before you run this template:

Create certificates for ELB and Cloudfront. It is recommended to use a wildcard certificate. For instance I created a ***.example.com** certificate.

Create the first certificate for ELB in the **`us-west-2`** region and the second certificate for Cloudfront **MUST** be in the **`us-east-1`**.

Make sure you add the certificates to your previously created zone

Use CNAME and add the names/values of the certificates

When the certificates are issued, you'd see the ARN field that looks similar to this:

```
ARN	arn:aws:acm:us-west-2:524289128064:certificate/631986d2-ee7b-4260-a83d-36cf62b2bba9
```
Note:

`ha-parameter.json` is the parameter file that will be used for the main High Availability WordPress set up. 

You can update the values as needed.

NOTE: For this parameter `WebServerKeyName` not to cause the CloudFormation stack provisioning to fail, enter an existing EC2 key you already have. 

Run the script:

```
aws cloudformation create-stack --stack-name HAWP --template-url https://kshah.s3-us-west-2.amazonaws.com/aws-cfn.yml  --parameters file://parameters.json --capabilities CAPABILITY_IAM
```

The stack created depends on the VPC and Zone stacks you created earlier. 